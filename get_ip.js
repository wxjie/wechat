var request = require('request');
var fs = require('fs');

var get_ip = function() {
    var token = fs_token.token();
    console.log(token);
    request("https://api.weixin.qq.com/cgi-bin/getcallbackip?access_token=" + token, function(error, res, data) {
        if (!error && res.statusCode == 200) {
            var _json = JSON.parse(data);
            console.log(_json);
        } else {
            console.error(error);
        }
    });
};

var fs_token = (function() {
    return {
        token: function() {
            var data = fs.readFileSync("data.text"); //同步读取文件内容
            return data.toString();
        }
    };
}());

get_ip();
