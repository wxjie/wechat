var request = require('request');
var fs = require("fs");
var moment = require('moment');

var get = function(appID,appsecret) {
    request("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="+appID+"&secret="+appsecret, function(error, res, data) {
        if (!error && res.statusCode == 200) {
            var _json = JSON.parse(data);
            save_data.set_token(_json.access_token, _json.expires_in);
        } else {
            console.log("请求失败: " + error);
        }
    });
};
get("wx6ffb79215c2e994c","27cb1251983b14a964304984d11f4e30");
var save_data = (function() {
    function set(token) {
        fs.writeFile("./data.text", token, function(err) { //异步写入
            if (err) {
                return console.error(err);
            }
            console.log("token 写入成功"+moment(new Date()).format("YYYY-MM-DD HH:mm:ss"));
        });
    }
    return {
        get_token: function() {
            var data = fs.readFileSync("data.text"); //同步读取
            return data.toString();
        },
        set_token: function(token, expires) {
            set(token);
            setTimeout(get, (expires - 100) * 1000);
        }
    };
}());
